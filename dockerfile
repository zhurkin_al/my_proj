FROM ubuntu:14.04
RUN apt update
RUN DEBIAN_FRONTEND=noninteractive apt install -y -q python-all python-pip
ADD ./my_proj/app/requirements.txt /tmp/requirements.txt
RUN DEBIAN_FRONTEND=noninteractive pip install -qr /tmp/requirements.txt
RUN DEBIAN_FRONTEND=noninteractive mkdir /opt/webapp
ADD ./my_proj/app/app.py /opt/webapp/app.py
EXPOSE 5000
WORKDIR /opt/webapp
CMD ["python", "app.py"]





